fn main() {
    let mut buf: String = String::new();
    let s: i32;
    let r1: i32;
    std::io::stdin().read_line(&mut buf).unwrap();

    let mut r1_and_s = buf.trim().split_ascii_whitespace();
    r1 = r1_and_s.next().unwrap().parse::<i32>().unwrap();
    s = r1_and_s.next().unwrap().parse::<i32>().unwrap();

    println!("{}", (s * 2 - r1).to_string());
}
